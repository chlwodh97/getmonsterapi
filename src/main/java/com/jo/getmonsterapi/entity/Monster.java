package com.jo.getmonsterapi.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Monster {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 15)
    private String MonsterName;

    @Column(nullable = false)
    private String imgTitle;

    @Column(nullable = false)
    private Integer monsterHp;

    @Column(nullable = false)
    private Integer monsterAttack;

    @Column(nullable = false)
    private String monsterType;
}
