package com.jo.getmonsterapi.service;


import com.jo.getmonsterapi.entity.Monster;
import com.jo.getmonsterapi.model.MonsterItem;
import com.jo.getmonsterapi.model.MonsterRequest;
import com.jo.getmonsterapi.model.MonsterResponse;
import com.jo.getmonsterapi.repository.MonsterRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MonsterService {
    private final MonsterRepository monsterRepository;


    public void setMonster(MonsterRequest request) {

        Monster addData = new Monster();
        addData.setImgTitle(request.getImgTitle());
        addData.setMonsterName(request.getMonsterName());
        addData.setMonsterHp(request.getMonsterHp());
        addData.setMonsterAttack(request.getMonsterAttack());
        addData.setMonsterType(request.getMonsterType());

        monsterRepository.save(addData);
    }

    public List<MonsterItem> getMonsters() {

        List<Monster> originList = monsterRepository.findAll();

        List<MonsterItem> result = new LinkedList<>();

        for (Monster monster : originList){
            MonsterItem addItem = new MonsterItem();
            addItem.setImgTitle(monster.getImgTitle());
            addItem.setMonsterName(monster.getMonsterName());
            addItem.setMonsterType(monster.getMonsterType());

            result.add(addItem);
        }
        return result;
    };

    public MonsterResponse getMonster(Long id) {

        Monster originData = monsterRepository.findById(id).orElseThrow();

        MonsterResponse response = new MonsterResponse();
        response.setId(originData.getId());
        response.setImgTitle(originData.getImgTitle());
        response.setMonsterName(originData.getMonsterName());
        response.setMonsterAttack(originData.getMonsterAttack());
        response.setMonsterHp(originData.getMonsterHp());
        response.setMonsterType(originData.getMonsterType());

        return response;

    }


}
