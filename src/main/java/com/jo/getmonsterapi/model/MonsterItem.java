package com.jo.getmonsterapi.model;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MonsterItem {

    private String MonsterName;
    private String imgTitle;
    private String monsterType;

}
