package com.jo.getmonsterapi.model;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommonResult {
    public String msg;
    public Integer code;
}
