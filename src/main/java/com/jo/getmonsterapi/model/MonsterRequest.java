package com.jo.getmonsterapi.model;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MonsterRequest {
    private String MonsterName;
    private String imgTitle;
    private Integer monsterHp;
    private Integer monsterAttack;
    private String monsterType;
}
