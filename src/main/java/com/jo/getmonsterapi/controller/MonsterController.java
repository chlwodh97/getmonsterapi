package com.jo.getmonsterapi.controller;


import com.jo.getmonsterapi.model.*;
import com.jo.getmonsterapi.service.MonsterService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/monster")
public class MonsterController {
    private final MonsterService monsterService;

    @PostMapping("/new")
    public CommonResult commonResult(@RequestBody MonsterRequest request) {
        monsterService.setMonster(request);

        CommonResult response = new CommonResult();
        response.setMsg("등록되었습니다");
        response.setCode(0);

        return response;
    };


    @GetMapping("list")
    public ListResult<MonsterItem> getMonsters(){
        List<MonsterItem> list = monsterService.getMonsters();

        ListResult<MonsterItem> response = new ListResult<>();
        response.setList(list);
        response.setTotalCount(list.size());
        response.setCode(0);
        response.setMsg("뽑아올게");

        return response;
    }

    @GetMapping("detail/{id}")
    public SingleResult<MonsterResponse> getMonster(@PathVariable long id){
        MonsterResponse data = monsterService.getMonster(id);

        SingleResult<MonsterResponse> result = new SingleResult<>();
        result.setData(data);
        result.setMsg("상세보기 여깄다");
        result.setCode(0);

        return result;
    }

}
