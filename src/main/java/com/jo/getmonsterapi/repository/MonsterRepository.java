package com.jo.getmonsterapi.repository;

import com.jo.getmonsterapi.entity.Monster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MonsterRepository extends JpaRepository<Monster , Long> {
}
