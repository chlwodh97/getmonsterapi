package com.jo.getmonsterapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GetMonsterApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GetMonsterApiApplication.class, args);
	}

}
